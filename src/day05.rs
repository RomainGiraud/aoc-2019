use aoc_toolbox::aoc_solver;
use crate::intcode;

#[aoc_solver("day05", "part1", "13087969")]
pub fn part1(input: String) -> String {
    intcode::Program::new(&input).run(&[1]).unwrap().to_string()
}

#[aoc_solver("day05", "part2", "14110739")]
pub fn part2(input: String) -> String {
    intcode::Program::new(&input).run(&[5]).unwrap().to_string()
}
