use aoc_toolbox::aoc_solver;

fn fuel(mass: i32) -> i32 {
    (mass / 3) - 2
}

fn total_fuel<I>(vals: I) -> i32
where
    I: IntoIterator<Item = i32>,
{
    let mut total = 0;
    for mass in vals.into_iter() {
        total += fuel(mass);
    }
    total
}

#[aoc_solver("day01", "part1", "3270717")]
pub fn part1(input: String) -> String {
    let v = input.lines()
        .map(|s| s.parse::<i32>())
        .filter_map(Result::ok);

    total_fuel(v).to_string()
}

fn fuel_r(mass: i32) -> i32 {
    return match fuel(mass) {
        m if m < 0 => 0,
        m => m + fuel_r(m),
    }
}

fn total_fuel_r<I>(vals: I) -> i32
where
    I: IntoIterator<Item = i32>,
{
    let mut total = 0;
    for mass in vals.into_iter() {
        total += fuel_r(mass);
    }
    total
}

#[aoc_solver("day01", "part2", "4903193")]
pub fn part2(input: String) -> String {
    let v = input.lines()
        .map(|s| s.parse::<i32>())
        .filter_map(Result::ok);

    total_fuel_r(v).to_string()
}
