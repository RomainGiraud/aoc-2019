use aoc_toolbox::aoc_solver;
use std::collections::HashMap;
use std::cell::RefCell;

struct Orbit {
    value: u32,
    base: Option<String>,
}

#[aoc_solver("day06", "part1", "142497")]
pub fn part1(input: String) -> String {
    let mut map = HashMap::<String, RefCell<Orbit>>::new();
    map.insert("COM".to_string(), RefCell::new(Orbit { value: 0, base: None }));

    // parse input
    for line in input.lines() {
        let t = line.trim().split(')').map(|s| String::from(s)).collect::<Vec<String>>();
        map.insert(t[1].to_string(), RefCell::new(match map.get(&t[0]) {
            None => {
                //println!("Unknown planet {}", t[0]);
                Orbit { value: 1, base: Some(t[0].to_string()) }
            },
            Some(v) => {
                let o = v.borrow();
                let r = match &o.base {
                    None => Orbit { value: o.value + 1, base: None },
                    Some(t) => Orbit { value: o.value + 1, base: Some(String::from(t)) },
                };
                //println!("planet {}: {}", t[1], r.value);
                r
            },
        }));
    }

    let mut total = 0;
    for (_planet, orbit) in &map {
        let mut v;
        {
            let mut curr = orbit.borrow();
            v = curr.value;
            while let Some(base) = &curr.base {
                curr = map.get(base).unwrap().borrow();
                v += curr.value;
            }
        }

        let mut o = orbit.borrow_mut();
        o.value = v;
        o.base = None;

        total += v;
    }

    total.to_string()
}

type Space = HashMap::<String, Option<String>>;

#[aoc_solver("day06", "part2", "301")]
pub fn part2(input: String) -> String {
    let mut map = Space::new();
    map.insert("COM".to_string(), None);

    // parse input
    for line in input.lines() {
        let t = line.trim().split(')').map(|s| String::from(s)).collect::<Vec<String>>();
        map.insert(t[1].to_string(), Some(t[0].to_string()));
    }

    let you = path("YOU", &map);
    let san = path("SAN", &map);

    let mut count = 0;
    for (it1, it2) in you.iter().rev().zip(san.iter().rev()) {
        if it1 != it2 {
            //println!("DIFF {} {}", it1, it2);
            break;
        } else {
            count += 1;
        }
    }
    (you.iter().rev().skip(count).count() + san.iter().rev().skip(count).count() - 2).to_string()
}

fn path<'a>(s: &'a str, map: &'a Space) -> Vec<&'a str> {
    let mut curr = s;
    let mut v = Vec::new();
    while curr != "COM" {
        v.push(curr);
        let g = map.get(curr).unwrap().as_ref();
        curr = g.unwrap().as_ref();
    }
    v
}
