use aoc_toolbox::aoc_solver;
use crate::intcode;

use permutohedron::Heap;

#[aoc_solver("day07", "part1", "38834")]
pub fn part1(input: String) -> String {
    let mut max = 0;
    let mut phases = vec![0,1,2,3,4];
    for data in Heap::new(&mut phases) {
        let mut last_output = 0;
        for i in data {
            last_output = intcode::Program::new(&input).run(&[i, last_output]).unwrap();
        }

        if last_output > max {
            max = last_output;
        }
    }

    max.to_string()
}

#[aoc_solver("day07", "part2", "69113332")]
pub fn part2(input: String) -> String {
    let mut programs = Vec::<intcode::Program>::with_capacity(5);
    for _ in 0..programs.capacity() {
        let mut p = intcode::Program::new(&input);
        p.set_stop_on_output(true);
        programs.push(p);
    }

    let mut max = 0;
    let mut phases = vec![5,6,7,8,9];
    for data in Heap::new(&mut phases) {
        let mut signal = 0;

        // load phase and process firt loop
        for (program, phase) in programs.iter_mut().zip(data) {
            signal = match program.run(&[phase, signal]) {
                None => panic!("no output"),
                Some(t) => t,
            };
        }

        loop {
            for program in programs.iter_mut() {
                signal = match program.run(&[signal, 0]) {
                    None => 0,
                    Some(t) => t,
                };

                if signal > max {
                    max = signal;
                }
            }

            if programs[programs.len() - 1].is_finished() {
                break;
            }
        }

        // reset each program
        for program in programs.iter_mut() {
            program.reset(&input);
            program.set_stop_on_output(true);
        }
    }

    max.to_string()
}
