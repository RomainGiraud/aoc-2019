use aoc_toolbox::aoc_solver;
use crate::intcode;

#[aoc_solver("day09", "part1", "2350741403")]
pub fn part1(input: String) -> String {
    let mut program = intcode::Program::new(&input);
    program.code.extend_from_slice(&[0; 2000]);
    program.run(&[1]).unwrap().to_string()
}

#[aoc_solver("day09", "part2", "53088")]
pub fn part2(input: String) -> String {
    let mut program = intcode::Program::new(&input);
    program.code.extend_from_slice(&[0; 2000]);
    program.run(&[2]).unwrap().to_string()
}
