use aoc_toolbox::aoc_solver;

// first exercice
fn is_valid(n: i32) -> bool {
    let mut n = n;
    let mut ten_power = 5;
    let mut prev = 0;
    let mut has_double = false;
    loop {
        let power = 10_i32.pow(ten_power as u32);
        let q = n / power;
        let r = n - q * power;
        //println!("{} = {} * {} + {}", n, q, power, r);
        n = r;

        if q < prev {
            return false;
        }

        if q == prev {
            has_double = true;
        }

        prev = q;

        ten_power -= 1;
        if ten_power < 0 {
            break;
        }
    }

    has_double
}

// second exercice
fn is_valid2(n: i32) -> bool {
    let mut n = n;
    let mut ten_power = 5;
    let mut prev = 0;
    let mut has_double = false;
    let mut n_double = -1;
    loop {
        let power = 10_i32.pow(ten_power as u32);
        let q = n / power;
        let r = n - q * power;
        //println!("{} = {} * {} + {}", n, q, power, r);
        n = r;

        if q < prev {
            return false;
        }

        if q == prev {
            if has_double {
                if q == n_double {
                    has_double = false;
                }
            } else {
                if n_double == -1 {
                    has_double = true;
                    n_double = q;
                } else if n_double != q {
                    has_double = true;
                    n_double = q;
                }
            }
        }
        //println!("has_double: {}", has_double);

        prev = q;

        ten_power -= 1;
        if ten_power < 0 {
            break;
        }
    }

    has_double
}

#[aoc_solver("day04", "part1", "1955")]
pub fn part1(input: String) -> String {
    let items: Vec<_> = input.trim().split('-').into_iter().map(|x| x.parse::<i32>().unwrap()).collect();
    let mut nb = 0;
    for i in items[0]..=items[1] {
        if is_valid(i) {
            nb += 1;
        }
    }
    nb.to_string()
}

#[aoc_solver("day04", "part2", "1319")]
pub fn part2(input: String) -> String {
    let items: Vec<_> = input.trim().split('-').into_iter().map(|x| x.parse::<i32>().unwrap()).collect();
    let mut nb = 0;
    for i in items[0]..=items[1] {
        if is_valid2(i) {
            nb += 1;
        }
    }
    nb.to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test01() {
        assert!(!is_valid2(111123));
        assert!(is_valid2(112233));
        assert!(!is_valid2(123444));
        assert!(is_valid2(111122));
        assert!(is_valid2(112233));
        assert!(!is_valid2(111222));
        assert!(is_valid2(112222));
    }
}
