use aoc_toolbox::aoc_solver;
use crate::intcode;

#[aoc_solver("day02", "part1", "3058646")]
pub fn part1(input: String) -> String {
    let mut program = intcode::Program::new(&input);
    program.code[1] = 12;
    program.code[2] = 2;
    program.run(&[1]);
    program.code[0].to_string()
}

#[aoc_solver("day02", "part2", "8976")]
pub fn part2(input: String) -> String {
    let program_orig = intcode::Program::new(&input);

    let mut noun = 0;
    let mut verb = 0;
    'outer: while noun < 100 {
        while verb < 100 {
            let mut prog = program_orig.clone();
            prog.code[1] = noun;
            prog.code[2] = verb;

            prog.run(&[]);

            if prog.code[0] == 19690720 {
                break 'outer;
            }

            verb += 1;
        }
        verb = 0;
        noun += 1;
    }

    (100 * noun + verb).to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn a_test01() {
        let mut p = intcode::Program::new("1,0,0,0,99");
        p.run(&[]);
        assert_eq!(p.code, &[2,0,0,0,99]);
    }

    #[test]
    fn a_test02() {
        let mut p = intcode::Program::new("2,3,0,3,99");
        p.run(&[]);
        assert_eq!(p.code, vec![2,3,0,6,99]);
    }

    #[test]
    fn a_test03() {
        let mut p = intcode::Program::new("2,4,4,5,99,0");
        p.run(&[]);
        assert_eq!(p.code, vec![2,4,4,5,99,9801]);
    }

    #[test]
    fn a_test04() {
        let mut p = intcode::Program::new("1,1,1,4,99,5,6,0,99");
        p.run(&[]);
        assert_eq!(p.code, vec![30,1,1,4,2,5,6,0,99]);
    }
}
