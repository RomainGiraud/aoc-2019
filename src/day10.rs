use aoc_toolbox::aoc_solver;
use std::collections::*;

fn equation<T>(p0: &(T, T), p1: &(T, T)) -> impl Fn(f32) -> f32 where
    T : std::ops::Sub + Copy + Into<f32> {
    let a : f32 = (p1.1.into() - p0.1.into()) / (p1.0.into() - p0.0.into());
    let b : f32 = p0.1.into() - a * p0.0.into();

    move |x| { a * x + b }
}

#[aoc_solver("day10", "part1", "278")]
pub fn part1(input: String) -> String {
    let mut space = HashSet::<(i16, i16)>::new();

    let mut max_size = 0;
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                space.insert((x as i16, y as i16));
            }

            if x > max_size {
                max_size = x;
            }
        }

        if y > max_size {
            max_size = y;
        }
    }

    let mut max = 0;
    for a in &space {
        let mut n = 0;

        for b in &space {
            if a == b {
                continue;
            }

            let mut visible = true;

            if a.0 == b.0 {
                for y in (a.1.min(b.1) + 1)..a.1.max(b.1) {
                    if space.contains(&(a.0, y))
                    {
                        visible = false;
                        break;
                    }
                }
            }

            let eq = equation(a, b);

            for x in (a.0.min(b.0) + 1)..a.0.max(b.0) {
                let yy = eq(x as f32);
                let y = yy.round() as i16;

                if (yy - y as f32).abs() > 0.0001_f32 {
                    // not an integer, cannot exists
                    continue;
                }

                if space.contains(&(x, y)) {
                    visible = false;
                    break;
                }
            }

            if visible {
                n += 1;
            }
        }

        if n > max {
            max = n;
        }
    }
    max.to_string()
}

#[aoc_solver("day10", "part2", "1417")]
pub fn part2(input: String) -> String {
    let mut space = HashSet::<(i16, i16)>::new();

    let mut max_size = 0;
    for (y, line) in input.lines().enumerate() {
        for (x, c) in line.chars().enumerate() {
            if c == '#' {
                space.insert((x as i16, y as i16));
            }

            if x > max_size {
                max_size = x;
            }
        }

        if y > max_size {
            max_size = y;
        }
    }

    let mut max = 0;
    let mut station = (-1, -1);
    for a in &space {
        let mut n = 0;

        for b in &space {
            if a == b {
                continue;
            }

            let mut visible = true;

            if a.0 == b.0 {
                for y in (a.1.min(b.1) + 1)..a.1.max(b.1) {
                    if space.contains(&(a.0, y))
                    {
                        visible = false;
                        break;
                    }
                }
            }

            let eq = equation(a, b);

            for x in (a.0.min(b.0) + 1)..a.0.max(b.0) {
                let yy = eq(x as f32);
                let y = yy.round() as i16;

                if (yy - y as f32).abs() > 0.0001_f32 {
                    // not an integer, cannot exists
                    continue;
                }

                if space.contains(&(x, y)) {
                    visible = false;
                    break;
                }
            }

            if visible {
                n += 1;
            }
        }

        if n > max {
            max = n;
            station = *a;
        }
    }

    space.remove(&station);

    let mut sorted = BTreeMap::<i32, Vec<&(i16, i16)>>::new();
    for ast in &space {
    //for ast in vec![&(11, 7), &(11, 3), &(12, 1), &(12, 2)] {
        let x = (ast.0 - station.0) as f32;
        let y = -(ast.1 - station.1) as f32;
        let m = (x * x + y * y).sqrt();
        let x = x / m;
        let y = y / m;

        let up_x = 0_f32;
        let up_y = 1_f32;

        let dot = up_x * x + up_y * y;
        let det = up_x * y - up_y * x;

        //println!("angle between UP and {}, {}", ast.0, ast.1);
        let angle = det.atan2(dot);
        //println!("  {}", angle);
        let angle = angle.to_degrees();
        //println!("  {}", angle);

        let mut angle = (360_f32 - angle) % 360_f32;
        if angle < 0_f32 {
            angle += 360_f32;
        }
        //println!("  {}", angle);
        let angle = (angle * 1000_f32) as i32;

        if let None = sorted.get(&angle) {
            sorted.insert(angle, Vec::new());
        }
        sorted.get_mut(&angle).unwrap().push(ast);
    }

    let mut idx = 0;
    let idx_target = 200;
    let mut target = (-1, -1);
    while idx != idx_target {
        let mut empty = true;
        for (_angle, ast) in sorted.iter_mut() {
            //println!("angle: {} => {:?}", angle, ast);
            let min = ast.iter().min_by(|a, b| {
                let x1 = a.0 - station.0;
                let y1 = a.1 - station.1;
                let x2 = b.0 - station.0;
                let y2 = b.1 - station.1;
                (x1 * x1 + y1 * y1).cmp(&(x2 * x2 + y2 * y2))
            });

            if min.is_none() {
                continue;
            }

            let min = min.unwrap();
            target = **min;
            //println!("angle: {}, min: {},{}", *angle as f32 / 1000 as f32, min.0, min.1);

            let pos = ast.iter().position(|x| *x == *min).unwrap();
            ast.remove(pos);

            empty = false;

            idx += 1;

            if idx == idx_target {
                break;
            }
        }

        if empty {
            break;
        }
    }

    //if idx == idx_target {
    //    println!("n°{} = ({}, {}) = {}", idx, target.0, target.1, target.0 * 100 + target.1);
    //} else {
    //    println!("ERROR: index {} not found ({})", idx_target, idx);
    //}
    //println!("{} {}", space.len(), sorted.len());

    (target.0 * 100 + target.1).to_string()
}
