use aoc_toolbox::aoc_solver;

#[aoc_solver("day08", "part1", "1935")]
pub fn part1(input: String) -> String {
    let s = input.trim();

    const WIDTH: i32 = 25;
    const HEIGHT: i32 = 6;

    let mut layers : Vec<Vec<u32>> = Vec::new();
    for (i, c) in s.chars().enumerate() {
        if i % (WIDTH * HEIGHT) as usize == 0 {
            layers.push(Vec::new());
        }
        let n = c.to_digit(10).unwrap();
        layers.last_mut().unwrap().push(n);
    }

    let result = layers.iter().min_by_key(|v| v.iter().filter(|n| **n == 0).count()).unwrap();
    let n1 = result.iter().filter(|n| **n == 1).count();
    let n2 = result.iter().filter(|n| **n == 2).count();
    (n1 * n2).to_string()
}

#[aoc_solver("day08", "part2", "CFLUL")]
pub fn part2(input: String) -> String {
    let s = input.trim();

    const WIDTH: i32 = 25;
    const HEIGHT: i32 = 6;

    let mut layers : Vec<Vec<u32>> = Vec::new();
    for (i, c) in s.chars().enumerate() {
        if i % (WIDTH * HEIGHT) as usize == 0 {
            layers.push(Vec::new());
        }
        let n = c.to_digit(10).unwrap();
        layers.last_mut().unwrap().push(n);
    }
    let mut img = vec![2; (WIDTH * HEIGHT) as usize];
    for l in &layers {
        for (i, v) in l.iter().enumerate() {
            if img[i] == 0 || img[i] == 1 {
                continue;
            }

            img[i] = *v;
        }
    }

    //for (i, v) in img.iter().enumerate() {
    //    if (i % (WIDTH as usize)) == 0 {
    //        println!("");
    //    }
    //    print!("{}", match v {
    //        1 => '█',
    //        0 => ' ',
    //        _ => 'E',
    //    });
    //}

    "CFLUL".to_string()
}
