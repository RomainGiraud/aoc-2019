#[derive(Clone, Debug)]
pub struct Program {
    pub code: Vec<i64>,
    index: usize,
    finished: bool,
    stop_on_output: bool,
    relative_base: i64,
}

impl Program {
    pub fn new(program: &str) -> Program {
        Program {
            code: program.trim().split(',').map(|i| i.parse::<i64>().unwrap()).collect(),
            index: 0,
            finished: false,
            stop_on_output: false,
            relative_base: 0,
        }
    }

    pub fn reset(&mut self, program: &str) {
        *self = Program::new(program)
    }

    pub fn is_finished(&self) -> bool {
        self.finished
    }

    pub fn set_stop_on_output(&mut self, value: bool) {
        self.stop_on_output = value;
    }

    fn select_param(&self, index: usize, param: i64) -> i64 {
        match param {
            0 => { // position mode
                self.code[self.code[index] as usize]
            },
            1 => { // immediate mode
                self.code[index]
            },
            2 => { // relative mode
                self.code[(self.relative_base + self.code[index]) as usize]
            },
            _ => panic!("mode unknown {}", param),
        }
    }

    fn set_value(&mut self, index: usize, param: i64, value: i64) {
        match param {
            0 => { // position mode
                let i = self.code[index] as usize;
                self.code[i] = value;
            },
            1 => { // immediate mode
                panic!("no immediate mode for writing");
            },
            2 => { // relative mode
                let i = (self.relative_base + self.code[index]) as usize;
                self.code[i] = value;
            },
            _ => panic!("mode unknown {}", param),
        };
    }

    pub fn run(&mut self, input: &[i64]) -> Option<i64> {
        let mut output = None;
        let mut params = input.into_iter();

        loop {
            let cmd = self.code[self.index];
            let opcode = cmd % 100;
            let mode_param0 = cmd / 100 % 10;
            let mode_param1 = cmd / 1000 % 10;
            let mode_param2 = cmd / 10000 % 10;
            self.index += 1; // command read
            //println!("{:?}", self.code);
            //println!("index: {}", self.index);
            //println!("opcode: {}", opcode);
            match opcode {
                1 => { // add
                    let param0 = self.select_param(self.index + 0, mode_param0);
                    let param1 = self.select_param(self.index + 1, mode_param1);

                    self.set_value(self.index + 2, mode_param2, param0 + param1);

                    self.index += 3;
                },
                2 => { // mult
                    let param0 = self.select_param(self.index + 0, mode_param0);
                    let param1 = self.select_param(self.index + 1, mode_param1);

                    self.set_value(self.index + 2, mode_param2, param0 * param1);

                    self.index += 3;
                },
                3 => { // input
                    let p = params.next();
                    assert!(!p.is_none());

                    self.set_value(self.index + 0, mode_param0, *(p.unwrap()));

                    self.index += 1;
                },
                4 => { // print
                    let param0 = self.select_param(self.index + 0, mode_param0);

                    output = Some(param0);

                    self.index += 1;

                    if self.stop_on_output {
                        break;
                    }
                },
                5 => { // jump-if-true
                    let param0 = self.select_param(self.index + 0, mode_param0);
                    let param1 = self.select_param(self.index + 1, mode_param1);

                    if param0 != 0 {
                        self.index = param1 as usize;
                    } else {
                        self.index += 2;
                    }
                },
                6 => { // jump-if-false
                    let param0 = self.select_param(self.index + 0, mode_param0);
                    let param1 = self.select_param(self.index + 1, mode_param1);

                    if param0 == 0 {
                        self.index = param1 as usize;
                    } else {
                        self.index += 2;
                    }
                },
                7 => { // less than
                    let param0 = self.select_param(self.index + 0, mode_param0);
                    let param1 = self.select_param(self.index + 1, mode_param1);

                    self.set_value(self.index + 2, mode_param2,
                        if param0 < param1 { 1 } else { 0 });

                    self.index += 3;
                },
                8 => { // equals
                    let param0 = self.select_param(self.index + 0, mode_param0);
                    let param1 = self.select_param(self.index + 1, mode_param1);

                    self.set_value(self.index + 2, mode_param2,
                        if param0 == param1 { 1 } else { 0 });

                    self.index += 3;
                },
                9 => { // adjusts relative base
                    let param0 = self.select_param(self.index + 0, mode_param0);

                    self.relative_base += param0;

                    self.index += 1;
                },
                99 => {
                    //println!("quit");
                    self.finished = true;
                    break;
                },
                _ => panic!("error"),
            }
        }

        output
    }
}
