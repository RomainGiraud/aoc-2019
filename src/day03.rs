use aoc_toolbox::aoc_solver;
use std::ops::{Add, Sub, Mul};
use std::fmt;
use std::mem;

#[derive(Debug, Copy, Clone, PartialEq)]
struct Point {
    x: f32,
    y: f32,
}

impl Point {
    fn new(x: f32, y: f32) -> Point {
        Point { x, y }
    }

    fn sqr_norm(self) -> f32 {
        self.x.abs() + self.y.abs()
    }

    fn cross_product(self, other: Self) -> f32 {
        self.x * other.y - self.y * other.x
    }
}

impl Add for Point {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Point {
    type Output = Self;

    fn sub(self, other: Self) -> Point {
        Point {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Mul for Point {
    type Output = Self;

    fn mul(self, other: Self) -> Point {
        Point {
            x: self.x * other.x,
            y: self.y * other.y,
        }
    }
}


#[derive(Debug, Copy, Clone)]
struct Line {
    start: Point,
    end: Point,
}

impl Line {
    fn new(start: Point, end: Point) -> Line {
        Line { start, end }
    }

    // https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect/565282#565282
    fn cross(&self, other: &Self) -> Option<Point> {
        let r = self.end - self.start;
        let s = other.end - other.start;
        let rxs = r.cross_product(s);
        let qpxr = (other.start - self.start).cross_product(r);

        if rxs.abs() < f32::EPSILON {
            if qpxr.abs() < f32::EPSILON {
                // TODO
                return None;
            } else {
                return None;
            }
        } else {
            let t = (other.start - self.start).cross_product(s) / rxs;
            let u = (other.start - self.start).cross_product(r) / rxs;

            if (0.0 <= t && t <= 1.0) && (0.0 <= u && u <= 1.0) {
                let xx = t * r.x;
                let yy = t * r.y;
                return Some(self.start + Point::new(xx, yy));
            }
        }

        None
    }

    fn intersect(&self, point: &Point) -> Option<i32> {
        let (p, mut min, mut max) = if self.start.x == self.end.x {
            if point.x != self.start.x {
                return None;
            }
            (point.y, self.start.y, self.end.y)
        } else /* if self.start.y == self.end.y */ {
            if point.y != self.start.y {
                return None;
            }
            (point.x, self.start.x, self.end.x)
        };

        let start = min;
        if min > max {
            mem::swap(&mut min, &mut max);
        }

        if p < min || p > max {
            return None;
        }

        Some((p - start).abs().round() as i32)
    }

    fn len(&self) -> i32 {
        if self.start.x == self.end.x {
            return (self.start.y - self.end.y).abs().round() as i32;
        }

        (self.start.x - self.end.x).abs().round() as i32
    }
}

#[derive(Debug, Clone)]
struct Wire {
    lines: Vec<Line>,
}

impl Wire {
    fn new(design: &str) -> Wire {
        let mut lines = Vec::new();
        let mut prev = Point::new(0.0, 0.0);
        for step in design.split(',') {
            let step = step.trim();
            let c = step.chars().next().unwrap();
            let dir = Self::parse_direction(c);
            let nb = step[1..].parse::<i32>().unwrap();

            let next = Point::new(prev.x + dir.0 * (nb as f32), prev.y + dir.1 * (nb as f32));
            lines.push(Line::new(prev, next.clone()));
            prev = next;
        }

        Wire {
            lines,
        }
    }

    fn parse_direction(c: char) -> (f32, f32) {
        match c {
            'R' => ( 1.0,  0.0),
            'L' => (-1.0,  0.0),
            'U' => ( 0.0,  1.0),
            'D' => ( 0.0, -1.0),
            _ => panic!("unknown direction: {}", c),
        }
    }

    fn intersections(&self, wire2: &Wire) -> Vec<Point> {
        let mut points = Vec::new();
        for l1 in self {
            for l2 in wire2.into_iter() {
                match l1.cross(l2) {
                    Some(p) => {
                        points.push(p);
                    },
                    None => {},
                }
            }
        }

        points
    }

    fn length_to(&self, point: &Point) -> Option<i32> {
        let mut total = 0;
        for line in &self.lines {
            match line.intersect(&point) {
                Some(length) => {
                    return Some(total + length);
                },
                None => {
                    total += line.len();
                },
            };
        }
        None
    }
}

impl fmt::Display for Wire {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "Wire: [")?;
        for l in self.lines.clone() {
            writeln!(f, " {}, {} => {}, {}", l.start.x, l.start.y, l.end.x, l.end.y)?;
        }
        write!(f, "]")
    }
}

struct WireIntoIter {
    wire: Wire,
    index: usize,
}

impl Iterator for WireIntoIter {
    type Item = Line;

    fn next(&mut self) -> Option<Line> {
        if self.index >= self.wire.lines.len() {
            return None;
        }

        let l = self.wire.lines[self.index];
        self.index += 1;
        return Some(l);
    }
}

impl IntoIterator for Wire {
    type Item = Line;
    type IntoIter = WireIntoIter;

    fn into_iter(self) -> Self::IntoIter {
        WireIntoIter {
            wire: self,
            index: 0,
        }
    }
}

impl<'a> IntoIterator for &'a Wire {
    type Item = &'a Line;
    type IntoIter = ::std::slice::Iter<'a, Line>;

    fn into_iter(self) -> Self::IntoIter {
        self.lines.iter()
    }
}

fn closest_intersection_dist(wire1: &str, wire2: &str) -> Option<i32> {
    let mut length: f32 = 0.0;
    let w1 = Wire::new(wire1);
    let w2 = Wire::new(wire2);
    for p in w1.intersections(&w2) {
        if length == 0.0 || p.sqr_norm() < length {
            length = p.sqr_norm();
        }
    }

    return if length < f32::EPSILON { None } else { Some(length as i32) };
}

#[aoc_solver("day03", "part1", "2180")]
pub fn part1(input: String) -> String {
    let mut lines = input.lines();
    closest_intersection_dist(lines.next().unwrap(), lines.next().unwrap()).unwrap().to_string()
}

fn closest_intersection_time(wire1: &str, wire2: &str) -> Option<i32> {
    let mut length: i32 = 0;
    let w1 = Wire::new(wire1);
    let w2 = Wire::new(wire2);
    for p in w1.intersections(&w2) {
        let l = w1.length_to(&p).unwrap() + w2.length_to(&p).unwrap();
        if length == 0 || l < length {
            length = l;
        }
    }

    return if length == 0 { None } else { Some(length) };
}

#[aoc_solver("day03", "part2", "112316")]
pub fn part2(input: String) -> String {
    let mut lines = input.lines();
    closest_intersection_time(lines.next().unwrap(), lines.next().unwrap()).unwrap().to_string()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn a_test01() {
        let w1 = "R8,U5,L5,D3";
        let w2 = "U7,R6,D4,L4";
        assert_eq!(closest_intersection_dist(w1, w2).unwrap(), 6);
    }

    #[test]
    fn a_test02() {
        let w1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72";
        let w2 = "U62,R66,U55,R34,D71,R55,D58,R83";
        assert_eq!(closest_intersection_dist(w1, w2).unwrap(), 159);
    }

    #[test]
    fn a_test03() {
        let w1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51";
        let w2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7";
        assert_eq!(closest_intersection_dist(w1, w2).unwrap(), 135);
    }

    #[test]
    fn b_test01() {
        let l1 = Line::new(Point::new(0.0, 0.0), Point::new(10.0, 0.0));
        let l2 = Line::new(Point::new(0.0, 1.0), Point::new(10.0, 1.0));
        let l3 = Line::new(Point::new(0.0, 0.0), Point::new(0.0, 10.0));
        let l4 = Line::new(Point::new(10.0, 0.0), Point::new(10.0, 10.0));
        let l5 = Line::new(Point::new(11.0, 0.0), Point::new(11.0, 10.0));
        assert_eq!(l1.cross(&l2), None);
        assert_eq!(l1.cross(&l3), Some(Point::new(0.0, 0.0)));
        assert_eq!(l1.cross(&l4), Some(Point::new(10.0, 0.0)));
        assert_eq!(l1.cross(&l5), None);
    }

    #[test]
    fn b_test02() {
        let l1 = Line::new(Point::new(-3274.0, -2759.0), Point::new(-3684.0, -2759.0));
        let l2 = Line::new(Point::new(-3668.0, -2054.0), Point::new(-3668.0, -3033.0));
        assert_eq!(l1.cross(&l2), Some(Point::new(-3668.0, -2759.0)));
    }

    #[test]
    fn b_test04() {
        let w1 = Wire::new("R4,U5");
        let w2 = Wire::new("U3,R5");
        let inters = w1.intersections(&w2);
        assert_eq!(inters.len(), 2);
        assert_eq!(inters[0], Point::new(0.0, 0.0));
        assert_eq!(inters[1], Point::new(4.0, 3.0));
    }

    #[test]
    fn b_test05() {
        let w = Wire::new("R4,U13");
        assert_eq!(w.lines.len(), 2);
        assert_eq!(w.lines[0].len(), 4);
        assert_eq!(w.lines[1].len(), 13);
        assert_eq!(w.lines[0].intersect(&Point::new(0.0, 0.0)), Some(0));
        assert_eq!(w.lines[0].intersect(&Point::new(4.0, 0.0)), Some(4));
        assert_eq!(w.lines[0].intersect(&Point::new(-1.0, 0.0)), None);
        assert_eq!(w.lines[0].intersect(&Point::new(5.0, 0.0)), None);
        assert_eq!(w.lines[0].intersect(&Point::new(0.0, 9.0)), None);
        assert_eq!(w.lines[1].intersect(&Point::new(4.0, 0.0)), Some(0));
        assert_eq!(w.lines[1].intersect(&Point::new(4.0, 1.0)), Some(1));
        assert_eq!(w.lines[1].intersect(&Point::new(4.0, 13.0)), Some(13));
        assert_eq!(w.lines[1].intersect(&Point::new(4.0, 14.0)), None);
    }

    #[test]
    fn b_test06() {
        let w = Wire::new("R8,U13,L6,D4");
        assert_eq!(w.length_to(&Point::new(1.0, 1.0)), None);
        assert_eq!(w.length_to(&Point::new(0.0, 0.0)), Some(0));
        assert_eq!(w.length_to(&Point::new(2.0, 11.0)), Some(8 + 13 + 6 + 2));
        assert_eq!(w.length_to(&Point::new(2.0, 9.0)), Some(8 + 13 + 6 + 4));
        assert_eq!(w.length_to(&Point::new(2.0, 8.0)), None);
    }

    #[test]
    fn b_test07() {
        let w1 = "R8,U5,L5,D3";
        let w2 = "U7,R6,D4,L4";
        assert_eq!(closest_intersection_time(w1, w2).unwrap(), 30);
    }

    #[test]
    fn b_test08() {
        let w1 = "R8,U5,L5,D2";
        let w2 = "U7,R6,D4,L3";
        assert_eq!(closest_intersection_time(w1, w2).unwrap(), 30);
    }

    #[test]
    fn b_test09() {
        let w1 = "R75,D30,R83,U83,L12,D49,R71,U7,L72";
        let w2 = "U62,R66,U55,R34,D71,R55,D58,R83";
        assert_eq!(closest_intersection_time(w1, w2).unwrap(), 610);
    }

    #[test]
    fn b_test10() {
        let w1 = "R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51";
        let w2 = "U98,R91,D20,R16,D67,R40,U7,R15,U6,R7";
        assert_eq!(closest_intersection_time(w1, w2).unwrap(), 410);
    }
}
